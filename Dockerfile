FROM registry.gitlab.ics.muni.cz:443/cryton/configurations/production-base:latest as base

# Set environment
ENV POETRY_VIRTUALENVS_IN_PROJECT=true

WORKDIR /app

# Install dependencies
COPY poetry.lock pyproject.toml ./
RUN poetry install --without dev --no-root --no-interaction --no-ansi

# Install app
COPY . /app/
RUN poetry install --only-root --no-interaction --no-ansi

FROM python:3.11-slim-bullseye as production

# Set environment
ENV CRYTON_CORE_APP_DIRECTORY=/app

# Copy app
COPY --from=base /app /app

# Make the executable accessible
RUN ln -s /app/.venv/bin/cryton-core /usr/local/bin/cryton-core

ENTRYPOINT [ "/app/docker/entrypoint.sh" ]
CMD [ "cryton-core", "start" ]

FROM httpd:2-alpine as proxy

COPY docker/apache-conf/httpd.conf /usr/local/apache2/conf/httpd.conf
COPY docker/apache-conf/hp-vhosts.conf /usr/local/apache2/conf/extra/httpd-vhosts.conf
COPY cryton_core/static/ /usr/local/apache2/web/static/

RUN chmod -R 755 /usr/local/apache2/web/static/
